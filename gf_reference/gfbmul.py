from nmigen_gf.reference.state import ST
from nmigen_gf.reference.decode_reducing_polynomial import decode_reducing_polynomial
from nmigen_gf.reference.clmul import clmul
from nmigen_gf.reference.cldivrem import cldivrem


def gfbmul(a, b):
    product = clmul(a, b)
    red_poly = decode_reducing_polynomial()
    q, r = cldivrem(product, red_poly, width=ST.XLEN + 1)
    return r
