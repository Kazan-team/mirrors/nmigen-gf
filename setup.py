from setuptools import setup, find_packages
import os

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, "README.md")).read()
NEWS = open(os.path.join(here, "NEWS.txt")).read()

version = "0.0.1"

install_requires = [
    # ok to install using pip3 as long as it is not with the rest of Libre-SOC.
    # https://git.libre-soc.org/?p=nmigen.git
    "nmigen",
    # can be obtained with pip3, best done manually
    # https://git.libre-soc.org/?p=nmutil.git
    "libresoc-nmutil",
]

test_requires = [
    "nose",
]

setup(
    name="libresoc-nmigen-gf",
    version=version,
    description="Reference Python code and nMigen HDL for Carry-Less and Galois Field Operations",
    long_description=README + "\n\n",
    long_description_content_type="text/markdown",
    classifiers=[
        "Topic :: Software Development",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    keywords="nmigen libre-soc carry-less clmul gf2 galois-field galois gfp",
    author="Jacob Lifshay",
    author_email="programmerjake@gmail.com",
    url="https://git.libre-soc.org/?p=nmigen-gf.git",
    license="LGPLv3+",
    packages=find_packages("src"),
    package_dir={"": "src"},
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires,
    tests_require=test_requires,
    test_suite="nose.collector"
)
