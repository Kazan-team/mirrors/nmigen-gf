# SPDX-License-Identifier: LGPL-3-or-later
# Copyright 2022 Jacob Lifshay programmerjake@gmail.com

# Funded by NLnet Assure Programme 2021-02-052, https://nlnet.nl/assure part
# of Horizon 2020 EU Programme 957073.

""" Carry-less Division and Remainder.

https://bugs.libre-soc.org/show_bug.cgi?id=784
"""

from nmigen.hdl.ast import Signal, Value, Assert
from nmigen.hdl.dsl import Module
from nmutil.singlepipe import ControlBase
from nmutil.clz import CLZ, clz
from nmutil.plain_data import plain_data, fields


def cldivrem_shifting(n, d, shape):
    """ Carry-less Division and Remainder based on shifting at start and end
        allowing us to get away with checking a single bit each iteration
        rather than checking for equal degrees every iteration.
        `n` and `d` are integers, `width` is the number of bits needed to hold
        each input/output.
        Returns a tuple `q, r` of the quotient and remainder.
    """
    assert isinstance(shape, CLDivRemShape)
    assert isinstance(n, int) and 0 <= n < 1 << shape.width
    assert isinstance(d, int) and 0 <= d < 1 << shape.width
    assert d != 0, "TODO: decide what happens on division by zero"

    # declare locals so nonlocal works
    r = q = shift = clock = substep = NotImplemented

    # functions match up to HDL parts:

    def set_to_initial():
        nonlocal d, r, q, clock, substep, shift
        # `clz(d, shape.width)`, but maxes out at `shape.width - 1` instead of
        # `shape.width` in order to both fit in `shape.shift_width` bits and
        # to not shift by more than needed.
        shift = clz(d >> 1, shape.width - 1)
        assert 0 <= shift < 1 << shape.shift_width, "shift overflow"
        d <<= shift
        assert 0 <= d < 1 << shape.d_width, "d overflow"
        r = n << shift
        assert 0 <= r < 1 << shape.r_width, "r overflow"
        q = 0
        clock = 0
        substep = 0

    def done():
        return clock == shape.done_clock

    def set_to_next():
        nonlocal r, q, clock, substep
        substep += 1
        substep %= shape.steps_per_clock
        if done():
            return
        elif substep == 0:
            clock += 1
        if clock == shape.width // shape.steps_per_clock \
                and substep >= shape.width % shape.steps_per_clock:
            clock = shape.done_clock
        q <<= 1
        r <<= 1
        if r >> (shape.width * 2 - 1) != 0:
            r ^= d << shape.width
            q |= 1
        assert 0 <= q < 1 << shape.q_width, "q overflow"
        assert 0 <= r < 1 << shape.r_width, "r overflow"

    def get_output():
        return q, (r >> shape.width) >> shift

    set_to_initial()

    # one clock-cycle per outer loop
    while not done():
        for expected_substep in range(shape.steps_per_clock):
            assert substep == expected_substep
            set_to_next()

    return get_output()


@plain_data(frozen=True, unsafe_hash=True)
class CLDivRemShape:
    __slots__ = "width", "steps_per_clock"

    def __init__(self, width, steps_per_clock=8):
        assert isinstance(width, int) and width >= 1, "invalid width"
        assert (isinstance(steps_per_clock, int)
                and steps_per_clock >= 1), "invalid steps_per_clock"
        self.width = width
        """bit-width of each of the carry-less div/rem inputs and outputs"""

        self.steps_per_clock = steps_per_clock
        """number of steps that should be taken per clock cycle"""

    @property
    def done_clock(self):
        """the clock tick number when iteration is finished
        -- the largest `CLDivRemState.clock` will get
        """
        if self.width % self.steps_per_clock == 0:
            return self.width // self.steps_per_clock
        return self.width // self.steps_per_clock + 1

    @property
    def clock_range(self):
        """the range that `CLDivRemState.clock` will fall in.

        returns: range
        """
        return range(self.done_clock + 1)

    @property
    def substep_range(self):
        """the range that `CLDivRemState.substep` will fall in.

        returns: range
        """
        return range(self.steps_per_clock)

    @property
    def d_width(self):
        """bit-width of the internal signal `CLDivRemState.d`"""
        return self.width

    @property
    def r_width(self):
        """bit-width of the internal signal `CLDivRemState.r`"""
        return self.width * 2

    @property
    def q_width(self):
        """bit-width of the internal signal `CLDivRemState.q`"""
        return self.width

    @property
    def shift_width(self):
        """bit-width of the internal signal `CLDivRemState.shift`"""
        return (self.width - 1).bit_length()


@plain_data(frozen=True, eq=False)
class CLDivRemState:
    __slots__ = "shape", "name", "clock", "substep", "d", "r", "q", "shift"
    def __init__(self, shape, *, name=None, src_loc_at=0):
        assert isinstance(shape, CLDivRemShape)
        if name is None:
            name = Signal(src_loc_at=1 + src_loc_at).name
        assert isinstance(name, str)
        self.shape = shape
        self.name = name
        self.clock = Signal(shape.clock_range, name=f"{name}_clock")
        self.substep = Signal(shape.substep_range, name=f"{name}_substep",
                              reset=0)
        self.d = Signal(shape.d_width, name=f"{name}_d")
        self.r = Signal(shape.r_width, name=f"{name}_r")
        self.q = Signal(shape.q_width, name=f"{name}_q")
        self.shift = Signal(shape.shift_width, name=f"{name}_shift")

    def eq(self, rhs):
        assert isinstance(rhs, CLDivRemState)
        for f in fields(CLDivRemState):
            if f in ("shape", "name"):
                continue
            l = getattr(self, f)
            r = getattr(rhs, f)
            yield l.eq(r)

    @staticmethod
    def like(other, *, name=None, src_loc_at=0):
        assert isinstance(other, CLDivRemState)
        return CLDivRemState(other.shape, name=name, src_loc_at=1 + src_loc_at)

    @property
    def done(self):
        return self.clock == self.shape.done_clock

    def get_output(self):
        return self.q, (self.r >> self.shape.width) >> self.shift

    def set_to_initial(self, m, n, d):
        assert isinstance(m, Module)
        n = Value.cast(n)  # convert to Value
        d = Value.cast(d)  # convert to Value
        clz_mod = CLZ(self.shape.width - 1)
        # can't name submodule since it would conflict if this function is
        # called multiple times in a Module
        m.submodules += clz_mod
        assert clz_mod.lz.width == self.shape.shift_width, \
            "internal inconsistency -- mismatched shift signal width"
        m.d.comb += [
            clz_mod.sig_in.eq(d >> 1),
            self.shift.eq(clz_mod.lz),
            self.d.eq(d << self.shift),
            self.r.eq(n << self.shift),
            self.q.eq(0),
            self.clock.eq(0),
            self.substep.eq(0),
        ]

    def eq_but_zero_substep(self, rhs, do_assert):
        assert isinstance(rhs, CLDivRemState)
        for f in fields(CLDivRemState):
            if f in ("shape", "name"):
                continue
            l = getattr(self, f)
            r = getattr(rhs, f)
            if f == "substep":
                if do_assert:
                    yield Assert(r == 0)
                r = 0
            yield l.eq(r)

    def set_to_next(self, m, state_in):
        assert isinstance(m, Module)
        assert isinstance(state_in, CLDivRemState)
        assert state_in.shape == self.shape
        assert self is not state_in, "a.set_to_next(m, a) is not allowed"
        width = self.shape.width
        substep_wraps = state_in.substep >= self.shape.steps_per_clock - 1
        with m.If(substep_wraps):
            m.d.comb += self.substep.eq(0)
        with m.Else():
            m.d.comb += self.substep.eq(state_in.substep + 1)

        with m.If(state_in.done):
            m.d.comb += [
                self.clock.eq(state_in.clock),
                self.d.eq(state_in.d),
                self.r.eq(state_in.r),
                self.q.eq(state_in.q),
                self.shift.eq(state_in.shift),
            ]
        with m.Else():
            clock = state_in.clock + substep_wraps
            with m.If((clock == width // self.shape.steps_per_clock)
                      & (self.substep >= width % self.shape.steps_per_clock)):
                m.d.comb += self.clock.eq(self.shape.done_clock)
            with m.Else():
                m.d.comb += self.clock.eq(clock)
            m.d.comb += [
                self.d.eq(state_in.d),
                self.shift.eq(state_in.shift),
            ]
            q = state_in.q << 1
            r = state_in.r << 1
            with m.If(r[width * 2 - 1]):
                m.d.comb += [
                    self.q.eq(q | 1),
                    self.r.eq(r ^ (state_in.d << width)),
                ]
            with m.Else():
                m.d.comb += [
                    self.q.eq(q),
                    self.r.eq(r),
                ]


class CLDivRemInputData:
    def __init__(self, shape):
        assert isinstance(shape, CLDivRemShape)
        self.shape = shape
        self.n = Signal(shape.width)
        self.d = Signal(shape.width)

    def __iter__(self):
        """ Get member signals. """
        yield self.n
        yield self.d

    def eq(self, rhs):
        """ Assign member signals. """
        return [
            self.n.eq(rhs.n),
            self.d.eq(rhs.d),
        ]


class CLDivRemOutputData:
    def __init__(self, shape):
        assert isinstance(shape, CLDivRemShape)
        self.shape = shape
        self.q = Signal(shape.width)
        self.r = Signal(shape.width)

    def __iter__(self):
        """ Get member signals. """
        yield self.q
        yield self.r

    def eq(self, rhs):
        """ Assign member signals. """
        return [
            self.q.eq(rhs.q),
            self.r.eq(rhs.r),
        ]

    def eq_output(self, state):
        assert isinstance(state, CLDivRemState)
        assert state.shape == self.shape
        q, r = state.get_output()
        return [self.q.eq(q), self.r.eq(r)]


class CLDivRemFSMStage(ControlBase):
    """carry-less div/rem

    Attributes:
    shape: CLDivRemShape
        the shape
    pspec:
        pipe-spec
    empty: Signal()
        true if nothing is stored in `self.saved_state`
    saved_state: CLDivRemState()
        the saved state that is currently being worked on.
    """

    def __init__(self, pspec, shape):
        assert isinstance(shape, CLDivRemShape)
        self.shape = shape
        self.pspec = pspec  # store now: used in ispec and ospec
        super().__init__(stage=self)
        self.empty = Signal(reset=1)
        self.saved_state = CLDivRemState(shape)

    def ispec(self):
        return CLDivRemInputData(self.shape)

    def ospec(self):
        return CLDivRemOutputData(self.shape)

    def setup(self, m, i):
        pass

    def elaborate(self, platform):
        m = super().elaborate(platform)
        i_data: CLDivRemInputData = self.p.i_data
        o_data: CLDivRemOutputData = self.n.o_data
        steps_per_clock = self.shape.steps_per_clock

        # TODO: handle cancellation

        m.d.comb += self.n.o_valid.eq(~self.empty & self.saved_state.done)
        m.d.comb += self.p.o_ready.eq(self.empty)

        def make_nc(i):
            return CLDivRemState(self.shape, name=f"next_chain_{i}")
        next_chain = [make_nc(i) for i in range(steps_per_clock + 1)]
        for i in range(steps_per_clock):
            next_chain[i + 1].set_to_next(m, next_chain[i])
        m.d.comb += next_chain[0].eq(self.saved_state)
        m.d.comb += o_data.eq_output(self.saved_state)
        initial_state = CLDivRemState(self.shape)
        initial_state.set_to_initial(m, n=i_data.n, d=i_data.d)

        do_assert = platform == "formal"

        with m.If(self.empty):
            m.d.sync += self.saved_state.eq_but_zero_substep(initial_state,
                                                             do_assert)
            with m.If(self.p.i_valid):
                m.d.sync += self.empty.eq(0)
        with m.Else():
            m.d.sync += self.saved_state.eq_but_zero_substep(next_chain[-1],
                                                             do_assert)
            with m.If(self.n.i_ready & self.n.o_valid):
                m.d.sync += self.empty.eq(1)
        return m

    def __iter__(self):
        yield from self.p
        yield from self.n

    def ports(self):
        return list(self)
